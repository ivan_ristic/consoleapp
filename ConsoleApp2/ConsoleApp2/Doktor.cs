﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
   public  class Doktor
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Specijalnost { get; set; }

        public List<Pacijent> Pacijenti { get; set; }

        public Doktor ()
        {
            this.Pacijenti = new List<Pacijent>();
        }

        public Doktor (int id, string ime, string specijalnost)
        {
            this.Id = id;
            this.Ime = ime;
            this.Specijalnost = specijalnost;
            this.Pacijenti = new List<Pacijent>();
        }

        public void ZakazivanjeKrvniPritisak (Pacijent pacijent)
        {
            KrvniPritisak krvniPritisak = new KrvniPritisak { Pacijent = pacijent, DatumIVreme = DateTime.Now};
        }

        public void ZakazivanjeNivoSecera(Pacijent pacijent)
        {
            NivoSecera nivoSecera = new NivoSecera { Pacijent = pacijent, DatumIVreme = DateTime.Now };
        }

    }
}
