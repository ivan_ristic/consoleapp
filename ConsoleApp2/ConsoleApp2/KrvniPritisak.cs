﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class KrvniPritisak : Pregled
    {
        public int GornjaVrednost { get; set; }
        public int DonjaVrednost { get; set; }
        public int Puls { get; set; }

        public KrvniPritisak ()
        {

        }

        public KrvniPritisak(int gornjaVrednost, int donjaVrednost, int puls, DateTime datumIVreme, Pacijent pacijent) : base(datumIVreme, pacijent)
        {
            this.GornjaVrednost = gornjaVrednost;
            this.DonjaVrednost = donjaVrednost;
            this.Puls = puls;
        }

        public override string ToString()
        {
            string ispis = "Pacijent: " + this.Pacijent.Ime + " , gornja granji pritisak iznosi: " + this.GornjaVrednost + " , donja vrednost: " + this.DonjaVrednost + " , puls: " + this.Puls;
            return ispis;
        }
    }
}
