﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class Pacijent
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string JMBG { get; set; }
        public int BrojKartona { get; set; }

        public Doktor Doktor { get; set; }

        public void UradiPregledSecer(Pacijent pacijent)
        {
            NivoSecera nivoSecera = new NivoSecera { Vrednost = 5, VremePoslednjegObroka = 14, Pacijent = pacijent };
            Console.WriteLine(nivoSecera.ToString());
        }

        public void UradiPregledPritisak(Pacijent pacijent)
        {
            KrvniPritisak pritisak = new KrvniPritisak { GornjaVrednost = 160, DonjaVrednost = 100, Puls = 60, Pacijent = pacijent};
            Console.WriteLine(pritisak.ToString());
        }
    }
}
