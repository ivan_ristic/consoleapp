﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class NivoHolesterola : Pregled
    {
        public int Vrednost { get; set; }
        public int VremePoslednjegObroka { get; set; }

        public NivoHolesterola(int vrednost, int vremePoslednjegObroka, DateTime datumIVreme, Pacijent pacijent) : base(datumIVreme, pacijent)
        {
            this.Vrednost = vrednost;
            this.VremePoslednjegObroka = vremePoslednjegObroka;
        }
    }
}
