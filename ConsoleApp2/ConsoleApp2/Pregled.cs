﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class Pregled
    {
        public DateTime DatumIVreme { get; set; }
        public Pacijent Pacijent { get; set; }

        public Pregled ()
        {

        }

        public Pregled (DateTime datumIVreme, Pacijent pacijent)
        {
            this.DatumIVreme = datumIVreme;
            this.Pacijent = pacijent;
        }
    }
}
