﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class NivoSecera : Pregled
    {
        public int Vrednost { get; set; }
        public int VremePoslednjegObroka { get; set; } 

        public NivoSecera ()
        {

        }

        public NivoSecera (int vrednost, int vremePoslednjegObroka, DateTime datumIVreme, Pacijent pacijent) : base(datumIVreme, pacijent)
        {
            this.Vrednost = vrednost;
            this.VremePoslednjegObroka = vremePoslednjegObroka;
        }

        public override string ToString()
        {
            string ispis = "Pacijent: " +this.Pacijent.Ime + " nivo secera iznosi : " + this.Vrednost + " , a vreme poslednjeg obroka : " + this.VremePoslednjegObroka + " sati";
            return ispis;
        }
        
    }
}
