﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //kreiranje pacijenta i doktora
            Doktor doktor = new Doktor { Id = 1, Ime = "Milan", Specijalnost = "Kardiolog" };
            Console.WriteLine("[" + DateTime.Now + "]" + " Kreiran doktor: " + "'"+ doktor.Ime + "'");
            Pacijent pacijent = new Pacijent { Id = 1, Ime = "Dragan", Prezime = "Ristic", JMBG = "1234567", BrojKartona = 4 };
            Console.WriteLine("[" + DateTime.Now + "]" + " Kreiran pacijent: " + "'" + pacijent.Ime + "'");

            //dodavanje doktora pacijentu
            pacijent.Doktor = doktor;
            Console.WriteLine("Pacijent " + "'" + pacijent.Ime + "'" + " je izabrao doktora " + "'" + doktor.Ime + "'");
            //zakazivanje termina
            doktor.ZakazivanjeNivoSecera(pacijent);
            doktor.ZakazivanjeKrvniPritisak(pacijent);   
            //pacijent radi analize
            pacijent.UradiPregledSecer(pacijent);
            pacijent.UradiPregledPritisak(pacijent);

            Console.ReadKey();
        }
    }
}
