﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3.Models
{
    public class KrvniPritisak : Pregled
    {
        public int Puls { get; set; }
        public int GornjaVrednost { get; set; }
        public int DonjaVrednost { get; set; }

        public KrvniPritisak ()
        {

        }

        public KrvniPritisak (int puls, int gornjaVrednost, int donjaVrednost, DateTime datumIVreme, Pacijent pacijent) : base (datumIVreme, pacijent)
        {
            this.Puls = puls;
            this.DonjaVrednost = donjaVrednost;
            this.GornjaVrednost = gornjaVrednost;
        }


        //Definisanje ispisa u konzoli
        public override string ToString()
        {
            string ispis = "Rezultati za pacijenta: " + this.Pacijent.Ime + ". Puls: " + this.Puls + ", donji pritisak: " + this.DonjaVrednost + ", gornja vrednost: " + this.GornjaVrednost; 
            return ispis;
        }
    }
}
