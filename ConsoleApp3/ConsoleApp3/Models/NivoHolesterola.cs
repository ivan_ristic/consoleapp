﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3.Models
{
    public class NivoHolesterola : Pregled
    {
        public decimal Vrednost { get; set; }
        public decimal VremePoslednjegObroka { get; set; }

        //Definisanje ispisa u konzoli
        public override string ToString()
        {
            string ispis = "Rezultati za pacijenta: " + this.Pacijent.Ime + ". Nivo holesterola je: " + this.Vrednost + ", vreme poslednjeg obroka je bilo u : " + this.VremePoslednjegObroka + " sati.";
            return ispis;
        }
    }
}
