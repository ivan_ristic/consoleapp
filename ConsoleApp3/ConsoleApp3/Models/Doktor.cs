﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3.Models
{
   public  class Doktor
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Specijalnost { get; set; }

        public List<Pacijent> Pacijenti { get; set; }

        public Doktor ()
        {
            this.Pacijenti = new List<Pacijent>();
        }

        public Doktor(int id, string ime, string specijalnost)
        {
            this.Id = id;
            this.Ime = ime;
            this.Specijalnost = specijalnost;
            this.Pacijenti = new List<Pacijent>();
        }


        //funkcije zakazaivanja koje doktor moze da obavlja

        public void ZakaziPregledNivoSecera(Pacijent pacijent)
        {
            Pregled pregled = new Pregled { DatumIVreme = DateTime.Now, Pacijent = pacijent };
        }

        public void ZakaziPregledKrvniPritisak(Pacijent pacijent)
        {
            Pregled pregled = new Pregled { DatumIVreme = DateTime.Now, Pacijent = pacijent };
        }
    }
}
