﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3.Models
{
    public class Pacijent
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string JMBG { get; set; }
        public int BrojKartona { get; set; }

        public Doktor Doktor { get; set; }

        //Obavljanje pregleda krvnog pritiska
        public void ObavljanjePregledaKP(KrvniPritisak krvniPritisak)
        {
            int puls = krvniPritisak.Puls;
            int donji = krvniPritisak.DonjaVrednost;
            int gornji = krvniPritisak.GornjaVrednost;
            Pacijent pacijent = krvniPritisak.Pacijent;
            DateTime datum = krvniPritisak.DatumIVreme;

            KrvniPritisak kp = new KrvniPritisak(puls, gornji, donji, datum, pacijent);
            //ispis
            Console.WriteLine(kp);
        }
        
        //Obavljanje pregleda secera u krvi
        public void ObavljanjePregledaSecera(NivoSecera nivoSecera)
        {
            decimal vrednost = nivoSecera.Vrednost;
            decimal vremePoslednjegObroka = nivoSecera.VremePoslednjegObroka;
            Pacijent pacijent = nivoSecera.Pacijent;
            DateTime datum = nivoSecera.DatumIVreme;

            NivoSecera ns = new NivoSecera(vrednost, vremePoslednjegObroka, datum, pacijent);
            //ispis
            Console.WriteLine(ns);
               
        }
    }
}
