﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3.Models
{
    public class NivoSecera : Pregled
    {
        public decimal Vrednost { get; set; }
        public decimal VremePoslednjegObroka { get; set; }


        public NivoSecera ()
        {

        }

        public NivoSecera (decimal vrednost, decimal vremePoslednjegObroka, DateTime datumIVreme, Pacijent pacijent) : base (datumIVreme, pacijent)
        {
            this.Vrednost = vrednost;
            this.VremePoslednjegObroka = vremePoslednjegObroka;
        }

        //Definisanje ispisa u konzoli
        public override string ToString()
        {
            string ispis = "Rezultati za pacijenta: " + this.Pacijent.Ime + ". Nivo secera u krvi je: " + this.Vrednost + ", vreme poslednjeg obroka je bilo u : " + this.VremePoslednjegObroka + " sati.";
            return ispis;
        }
    }
}
