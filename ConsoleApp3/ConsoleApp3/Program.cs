﻿using ConsoleApp3.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        
        static void Main(string[] args)
        {
            //Kreiranje logera
            Logger log = new Logger();

            //Kreiranje doktora
            Doktor doktor = new Doktor { Id = 1, Ime = "Milan", Specijalnost = "Kardiolog" };
            log.Log(doktor);

            //Kreiranje pacijenta
            Pacijent pacijent = new Pacijent { Id = 1, Ime = "Dragan", Prezime = "Petrovic", JMBG = "1234567890123", BrojKartona = 123 };
            log.Log(pacijent);

            //Dodela doktora pacijentu
            pacijent.Doktor = doktor;
            log.Log(doktor, pacijent);

            //Zakazivanje pregleda
            doktor.ZakaziPregledKrvniPritisak(pacijent);

            //Zakazivanje pregleda 
            doktor.ZakaziPregledNivoSecera(pacijent);

            //Obavljanje pregleda krvnog pritiska
            pacijent.ObavljanjePregledaKP(new KrvniPritisak { Puls = 80, DonjaVrednost = 80, GornjaVrednost = 130, DatumIVreme = DateTime.Now, Pacijent = pacijent });
            log.LogPregled(pacijent);
            //Obavljanje pregleda nivoa secera u krvi
            pacijent.ObavljanjePregledaSecera(new NivoSecera { Vrednost = 5.8m, VremePoslednjegObroka = 12.30m, DatumIVreme = DateTime.Now, Pacijent = pacijent });
            log.LogPregled(pacijent);

            Console.ReadLine();
        }
    }
}
