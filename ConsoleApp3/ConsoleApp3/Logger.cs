﻿using ConsoleApp3.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    public class Logger
    {
        public static void VerifyDir(string path)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(path);
                if (!dir.Exists)
                {
                    dir.Create();
                }
            }
            catch { }
        }

        public void Log(Pacijent pacijent)
        {
            string path = "../../Log/";
            VerifyDir(path);
            string fileName = DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + "_Logs.txt";
            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(path + fileName, true);
                file.WriteLine("[" + DateTime.Now.ToString() + "]" + " Kreiran je pacijent " + pacijent.Ime);
                file.Close();
            }
            catch (Exception) { }
        }

        public void Log(Doktor doktor)
        {
            string path = "../../Log/"; ;
            VerifyDir(path);
            string fileName = DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + "_Logs.txt";
            //string fileName = DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + "_Logs.txt";
            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(path + fileName, true);
                file.WriteLine("[" + DateTime.Now.ToString() + "]" + " Kreiran je doktor " + doktor.Ime);
                file.Close();
            }
            catch (Exception) { }
        }

        public void Log(Doktor doktor, Pacijent pacijent)
        {
            string path = "../../Log/";
            VerifyDir(path);
            string fileName = DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + "_Logs.txt";
            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(path + fileName, true);
                file.WriteLine("[" + DateTime.Now.ToString() + "] Pacijent " + pacijent.Ime + " je izabrao doktora " + doktor.Ime + ".");
                file.Close();
            }
            catch (Exception) { }
        }

        public void LogPregled(Pacijent pacijent)
        {
            string path = "../../Log/";
            VerifyDir(path);
            string fileName = DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + "_Logs.txt";
            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(path + fileName, true);
                file.WriteLine("[" + DateTime.Now.ToString() + "] Pacijent " + pacijent.Ime + " je obavio lekarski pregled.");
                file.Close();
            }
            catch (Exception) { }
        }
    }
}
